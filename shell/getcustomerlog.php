<?php

// \Magento\Webapi\Model\ServiceMetadata::getServicesConfig to debug the available webapi services
$params = ['email' => null];
foreach ($argv as $arg) {
    if (preg_match("%^--(.*?)=(.*?)$%", $arg, $m)) {
        $params[$m[1]] = $m[2];
    }
}

if (!$params['email'])
    exit("Specify customer id (as --email=_EMAIL_ parameter)\n");

$email   = (string)$params['email'];
$url = 'http://magecertif.test/index.php/rest/V1/lastcustomerlog/' . $email;

/*$ch = curl_init();
curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Authorization: Bearer or7ozb3e97x19nk97zylmhspul7niqi3'
    )
);

//curl_setopt($ch,CURLOPT_POSTFIELDS, $str);

$result = curl_exec($ch);
curl_close($ch);

$result = json_decode($result);*/

$proxy = new SoapClient('http://magecertif.test/index.php/soap/default?wsdl&services=mbsBackendScreenCustomerLogRepositoryV1');
$result = $proxy->mbsBackendScreenCustomerLogRepositoryV1GetLastLogByCustomerEmail(["email"=> $email]);

print_r($result);