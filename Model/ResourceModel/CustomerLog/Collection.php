<?php

namespace Mbs\BackendScreen\Model\ResourceModel\CustomerLog;

use Mbs\BackendScreen\Model\CustomerLog;
use Mbs\BackendScreen\Model\ResourceModel\CustomerLog as CustomerLogResource;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(CustomerLog::class, CustomerLogResource::class);
    }

}