<?php

namespace Mbs\BackendScreen\Model\ResourceModel;

class CustomerLog extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init('mbs_customer_logs', 'id');
    }
}