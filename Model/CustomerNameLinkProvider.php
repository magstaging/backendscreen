<?php

namespace Mbs\BackendScreen\Model;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

class CustomerNameLinkProvider
{
    /**
     * @var \Magento\Framework\Api\FilterBuilder
     */
    private $filterBuilder;
    /**
     * @var \Mbs\BackendScreen\Api\Data\CustomerDataInterfaceFactory
     */
    private $customerDataInterfaceFactory;
    /**
     * @var \Mbs\BackendScreen\Api\Data\CustomerLogExtensionInterfaceFactory
     */
    private $customerLogExtensionInterfaceFactory;
    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    public function __construct(
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        \Mbs\BackendScreen\Api\Data\CustomerDataInterfaceFactory $customerDataInterfaceFactory,
        \Mbs\BackendScreen\Api\Data\CustomerLogExtensionInterfaceFactory $customerLogExtensionInterfaceFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->filterBuilder = $filterBuilder;
        $this->customerDataInterfaceFactory = $customerDataInterfaceFactory;
        $this->customerLogExtensionInterfaceFactory = $customerLogExtensionInterfaceFactory;
        $this->customerRepository = $customerRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @throws NoSuchEntityException
     */
    public function assignCustomerNameToCollection($collection)
    {
        if ($collection->count()>0) {
            $filter = $this->filterBuilder->setField('entity_id')
                ->setConditionType('in')
                ->setValue($collection->getColumnValues('customer_id'))
                ->create();
            $searchCriteria = $this->searchCriteriaBuilder->addFilters([$filter])
                ->create();

            try {
                $customerList = $this->customerRepository->getList($searchCriteria)->getItems();

                /** @var \Mbs\BackendScreen\Model\CustomerLog $customerLog */
                foreach ($collection as $customerLog) {
                    /** @var \Magento\Customer\Model\Data\Customer $customer */
                    if ($customer = $this->findCustomerRecord($customerList, $customerLog)) {
                        if ($customerLog->getExtensionAttributes() == null) {
                            /** @var \Mbs\BackendScreen\Api\Data\CustomerLogExtensionInterface $extension */
                            $extension = $this->customerLogExtensionInterfaceFactory->create();
                        } else {
                            $extension = $customerLog->getExtensionAttributes();
                        }
                        $customerLog->setExtensionAttributes($extension);
                        $extension->setCustomerName($this->getCustomerName($customer));
                        /** @var \Mbs\BackendScreen\Api\Data\CustomerDataInterface $customerData */
                        $customerData = $this->customerDataInterfaceFactory->create();
                        $customerData->setCustomerName($this->getCustomerName($customer));
                        $extension->setCustomerData($customerData);
                    }
                }
            } catch (LocalizedException $e) {
                throw new NoSuchEntityException(__('No Customer found for the customer logs'));
            }
        }
    }

    private function findCustomerRecord(array $customerList, $customerLog)
    {
        foreach ($customerList as $customer) {
            if ($customerLog->getCustomerId() === $customer->getId()) {
                return $customer;
            }
        }
    }

    /**
     * @param \Magento\Customer\Model\Data\Customer $customer
     * @return string
     */
    private function getCustomerName(\Magento\Customer\Model\Data\Customer $customer): string
    {
        return sprintf('%s %s', $customer->getFirstname(), $customer->getLastname());
    }
}
