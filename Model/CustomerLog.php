<?php

namespace Mbs\BackendScreen\Model;

use Mbs\BackendScreen\Api\Data\CustomerLogInterface;

class CustomerLog extends \Magento\Framework\Model\AbstractExtensibleModel
    implements \Mbs\BackendScreen\Api\Data\CustomerLogInterface
{
    protected function _construct()
    {
        $this->_init(ResourceModel\CustomerLog::class);
    }

    /**
     * @inheritDoc
     */
    public function getCustomerId()
    {
        return $this->_getData('customer_id');
    }

    /**
     * @inheritDoc
     */
    public function setCustomerId($id)
    {
        $this->setData('customer_id', $id);
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt()
    {
        return $this->_getData('created_at');
    }

    /**
     * @inheritDoc
     */
    public function setCreatedAt($date)
    {
        $this->setData('created_at', $date);
    }

    /**
     * @inheritDoc
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * @inheritDoc
     */
    public function setExtensionAttributes(\Mbs\BackendScreen\Api\Data\CustomerLogExtensionInterface $extensionAttributes)
    {
        $this->_setExtensionAttributes($extensionAttributes);
    }
}