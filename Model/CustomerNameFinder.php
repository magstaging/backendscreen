<?php

namespace Mbs\BackendScreen\Model;

use Magento\Customer\Model\ResourceModel\CustomerRepository;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

class CustomerNameFinder
{
    /**
     * @var CustomerRepository
     */
    private $customerRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    public function __construct(
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder
    ) {
        $this->customerRepository = $customerRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
    }

    public function getCustomerName(\Mbs\BackendScreen\Api\Data\CustomerLogInterface $customerLog)
    {
        try {
            $customer = $this->customerRepository->getById($customerLog->getCustomerId());
            $name = sprintf('%s %s', $customer->getFirstname(), $customer->getLastname());
        } catch (NoSuchEntityException $e) {
            $name = 'removed customer';
        } catch (LocalizedException $e) {
            $name = 'removed customer';
        }

        return $name;
    }
}
