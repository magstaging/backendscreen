<?php

namespace Mbs\BackendScreen\Model;

use Magento\Framework\Exception\NoSuchEntityException;

class CustomerLogRepository implements \Mbs\BackendScreen\Api\CustomerLogRepositoryInterface
{
    /**
     * @var ResourceModel\CustomerLog\CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface
     */
    private $collectionProcessor;
    /**
     * @var \Magento\Framework\Api\SearchCriteriaInterface
     */
    private $criteria;
    /**
     * @var CustomerNameLinkProvider
     */
    private $customerNameLinkProvider;

    public function __construct(
        \Mbs\BackendScreen\Model\ResourceModel\CustomerLog\CollectionFactory $collectionFactory,
        \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface $collectionProcessor,
        \Magento\Framework\Api\SearchCriteriaInterface $criteria,
        \Mbs\BackendScreen\Model\CustomerNameLinkProvider $customerNameLinkProvider
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->criteria = $criteria;
        $this->customerNameLinkProvider = $customerNameLinkProvider;
    }

    /**
     * @inheritDoc
     * @throws NoSuchEntityException
     */
    public function getLastLogByCustomerEmail($email)
    {
        /** @var \Mbs\BackendScreen\Model\ResourceModel\CustomerLog\Collection $collection */
        $collection = $this->initialiseCustomerLogWithEmail($email);
        $collection->setOrder('created_at', \Magento\Framework\Data\Collection::SORT_ORDER_DESC);
        $collection->setPageSize(1);

        if ($collection->count()==0) {
            throw new NoSuchEntityException(__('The Customer email does not have customer logs.', $email));
        }

        $this->customerNameLinkProvider->assignCustomerNameToCollection($collection);

        return $collection->getFirstItem();
    }

    /**
     * @inheritDoc
     * @throws NoSuchEntityException
     */
    public function getListLogByCustomerEmail($email)
    {
        $collection = $this->initialiseCustomerLogWithEmail($email);

        if ($collection->count()==0) {
            throw new NoSuchEntityException(__('The Customer email does not have customer logs.', $email));
        }

        return $collection->getItems();
    }

    /**
     * @param $email
     * @return ResourceModel\CustomerLog\Collection
     */
    private function initialiseCustomerLogWithEmail($email): ResourceModel\CustomerLog\Collection
    {
        /** @var \Mbs\BackendScreen\Model\ResourceModel\CustomerLog\Collection $collection */
        $collection = $this->collectionFactory->create();
        $join[] = 'customer.entity_id=main_table.customer_id';
        $join[] = $collection->getSelect()->getConnection()->quoteInto('customer.email=?', $email);
        $collection->join(
            ['customer' => $collection->getTable('customer_entity')],
            implode(' and ', $join),
            ''
        );

        return $collection;
    }
}
