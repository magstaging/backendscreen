<?php

namespace Mbs\BackendScreen\Ui\Component;

use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\ReportingInterface;
use Magento\Framework\Api\Search\SearchCriteriaBuilder;
use Magento\Framework\App\RequestInterface;

class DataProvider extends \Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider
{
    /**
     * @var \Magento\Framework\Serialize\SerializerInterface
     */
    private $serializer;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        ReportingInterface $reporting,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        RequestInterface $request,
        FilterBuilder $filterBuilder,
        \Magento\Framework\Serialize\SerializerInterface $serializer,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $reporting, $searchCriteriaBuilder, $request, $filterBuilder, $meta, $data);
        $this->serializer = $serializer;
    }

    public function getData()
    {
        $data = parent::getData();

        $data = $this->encodeCustomerNameToSimulateStackExchangeQuestion($data); // this line is not needed if the data to split is already in json format

        foreach ($data['items'] as &$item) {
            $item['customer_firstname'] = $this->extractKeyValueFromJsonCustomerData($item['customer_name'], 'firstname');
            $item['customer_lastname'] = $this->extractKeyValueFromJsonCustomerData($item['customer_name'], 'lastname');
        }

        return $data;
    }

    private function extractKeyValueFromJsonCustomerData($jsonCustomerInfo, $key)
    {
        $data = $this->serializer->unserialize($jsonCustomerInfo);
        return isset($data[$key])?$data[$key]:'unknown';
    }

    private function encodeCustomerNameToSimulateStackExchangeQuestion(array $data)
    {
        foreach ($data['items'] as &$item) {
            $customerName = $item['extension_attributes']->getCustomerName();
            if (preg_match("%^(.*) (.*)$%", $customerName, $m)) {
                $item['customer_name'] = $this->serializer->serialize([
                    'firstname' => $m[1],
                    'lastname' => $m[2]
                ]);
            }
        }

        return $data;
    }
}