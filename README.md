# README #

Add a backend screen that shows customer logs data. This screen is only for study purpose and does not
create useful data.

### How do I get set up? ###

1. clone the repository
2. create folder app/code/Mbs/BackendScreen when located at the root of the Magento site
3. copy the content of this repository within the folder
4. install the module php bin/magento setup:upgrade

you may have to insert some rows in the database manually if you need data to show.
Go to content/customer log in the backend