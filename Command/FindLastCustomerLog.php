<?php

namespace Mbs\BackendScreen\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FindLastCustomerLog extends Command
{
    /**
     * @var \Mbs\BackendScreen\Model\CustomerLogRepository
     */
    private $customerLogRepository;

    public function __construct(
        \Mbs\BackendScreen\Model\CustomerLogRepository $customerLogRepository,
        string $name = null
    ) {
        parent::__construct($name);
        $this->customerLogRepository = $customerLogRepository;
    }

    protected function configure()
    {
        $this->setName('mbs:customerlog:findlast');
        $this->setDescription('Find last customer log for an email');

        $this->addArgument('email', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $customerLog = $this->customerLogRepository->getLastLogByCustomerEmail($input->getArgument('email'));

        $output->writeln(sprintf('log: %s, %s (%s)', $customerLog->getCustomerId(), $customerLog->getExtensionAttributes()->getCustomerName(), $customerLog->getCreatedAt()));
        $output->writeln('Found last customer log');
    }
}