<?php

namespace Mbs\BackendScreen\Api\Data;

interface CustomerDataInterface
{
    /**
     * @return string
     */
    public function getCustomerName();

    /**
     * @param string $name
     */
    public function setCustomerName($name);
}