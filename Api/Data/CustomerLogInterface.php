<?php

namespace Mbs\BackendScreen\Api\Data;

interface CustomerLogInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    /**
     * @return int
     */
    public function getCustomerId();

    /**
     * @param int $id
     * @return void
     */
    public function setCustomerId($id);

    /**
     * @return string
     */
    public function getCreatedAt();

    /**
     * @param string $date
     * @return void
     */
    public function setCreatedAt($date);

    /**
     * Retrieve existing extension attributes object or create a new one.
     *
     * @return \Mbs\BackendScreen\Api\Data\CustomerLogExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     *
     * @param \Mbs\BackendScreen\Api\Data\CustomerLogExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(\Mbs\BackendScreen\Api\Data\CustomerLogExtensionInterface $extensionAttributes);
}
