<?php

namespace Mbs\BackendScreen\Api;

interface CustomerLogRepositoryInterface
{
    /**
     * @param string $email
     * @return \Mbs\BackendScreen\Api\Data\CustomerLogInterface
     */
    public function getLastLogByCustomerEmail($email);

    /**
     * @param string $email
     * @return \Mbs\BackendScreen\Api\Data\CustomerLogInterface[]
     */
    public function getListLogByCustomerEmail($email);
}
