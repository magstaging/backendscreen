<?php

namespace Mbs\BackendScreen\Plugin;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\UiComponent\DataProvider\CollectionFactory;

class JoinCustomerName
{
    /**
     * @var \Mbs\BackendScreen\Model\CustomerNameLinkProvider
     */
    private $customerNameLinkProvider;

    public function __construct(
        \Mbs\BackendScreen\Model\CustomerNameLinkProvider $customerNameLinkProvider
    ) {
        $this->customerNameLinkProvider = $customerNameLinkProvider;
    }

    public function afterGetReport(
        CollectionFactory $subject,
        $collection,
        $requestName
    ) {
        if ($requestName == 'customerlog_listing_data_source') {
            try {
                $this->customerNameLinkProvider->assignCustomerNameToCollection($collection);
            } catch (NoSuchEntityException $e) {
            }
        }
        return $collection;
    }
}