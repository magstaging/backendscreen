<?php

namespace Mbs\BackendScreen\Plugin;

class AssignCustomerName
{
    /**
     * @var \Mbs\BackendScreen\Api\Data\CustomerLogExtensionInterfaceFactory
     */
    private $customerLogExtension;
    /**
     * @var \Mbs\BackendScreen\Model\CustomerNameFinder
     */
    private $customerNameFinder;

    public function __construct(
        \Mbs\BackendScreen\Api\Data\CustomerLogExtensionInterfaceFactory $customerLogExtension,
        \Mbs\BackendScreen\Model\CustomerNameFinder $customerNameFinder
    ) {
        $this->customerLogExtension = $customerLogExtension;
        $this->customerNameFinder = $customerNameFinder;
    }

    public function afterGet(
        \Magento\Customer\Api\CustomerRepositoryInterface $subject,
        \Mbs\BackendScreen\Api\Data\CustomerLogInterface $customerLog
    ) {
        $extensionAttributes = $this->getExtensionAttribute($customerLog);

        $extensionAttributes->setData(
            'customer_name',
            $this->customerNameFinder->getCustomerName($customerLog)
        );
    }

    private function getExtensionAttribute(\Mbs\BackendScreen\Api\Data\CustomerLogInterface $customerLog)
    {
        $extensionAttributes = $customerLog->getExtensionAttributes();
        if ($extensionAttributes === null) {
            $extensionAttributes = $this->customerLogExtension->create();
        }

        return $extensionAttributes;
    }
}